Annotations and their Intepretations

1. ALoFT: Annotation of Loss-of-Function Transcripts
Annotates putative loss of Function variants in proteing-colding including functional, evolutionary and network features. Further, ALoFT can predict the impact of premature stop variants.
classifies putative loss of function variants into three classes: those that are benign, those that lead to recessive disease (disease-causing only when homozygous) and those that lead to dominant disease (disease-causing as heterozygotes).
openCRAVAT entries into a vcf file:
##INFO=<ID=OC_aloft__transcript,Number=A,Type=String,Description="Ensembl transcript ids">
##INFO=<ID=OC_aloft__affect,Number=A,Type=String,Description="the fraction of the transcripts of the gene affected i.e. No. of transcripts affected by the SNP/Total no. of protein_coding transcripts for the gene.">
##INFO=<ID=OC_aloft__tolerant,Number=A,Type=Float,Description="Probability of the SNP being classified as benign by ALoFT">
##INFO=<ID=OC_aloft__recessive,Number=A,Type=Float,Description="Probability of the SNP being classified as recessive disease-causing by ALoFT">
##INFO=<ID=OC_aloft__dominant,Number=A,Type=Float,Description="Probability of the SNP being classified as dominant disease-causing by ALoFT">
##INFO=<ID=OC_aloft__pred,Number=A,Type=String,Description="final classification predicted by ALoFT; values can be Tolerant, Recessive or Dominant">
##INFO=<ID=OC_aloft__conf,Number=A,Type=String,Description="Confidence level of Aloft_pred; values can be "High Confidence" (p < 0.05) or "Low Confidence" (p > 0.05)">
##INFO=<ID=OC_aloft__all,Number=A,Type=String,Description="">

These can be filtered using bcftools: bcftools query -f  '%CHROM %POS %OC_aloft__tolerant %OC_aloft__recessive %OC_aloft__dominant %OC_aloft__pred %OC_aloft__conf%\n' vcf_file.vcf

2. Combined Annotation Dependent Depletion (CADD)
This is used to score the deleteriousness of single nucleotide variants as well as insertion/deletions variants in the human genome.
openCRAVAT entries into a vcf file:
##INFO=<ID=OC_cadd_exome__score,Number=A,Type=Float,Description="">
##INFO=<ID=OC_cadd_exome__phred,Number=A,Type=Float,Description="">
The OC_cadd_exome__score: negative values, suggests extent to which the variant is neutral, Positive values suggests extent to which variant is deleterious 
The OC_cadd_exome__phred: scaled scores,  a scaled score of 10 or greater indicates a raw score in the top 10% deleterious variants in the human genome, and a score of 20 or greater indicates a raw score in the top 1% deleterious variants in the human genome.****
filtering example: bcftools query ../OC_Reports/ocweb_chr21_swn_patient_1_blood.vcf.gz -f  '%CHROM %POS %OC_cadd_exome__score %OC_cadd_exome__phred\n' 

3. VEST4 
Is used to predict the functional impact of single nucleotide polymorphisms (SNPs) in protein-coding genes. VEST assigns a score to each SNP based on its predicted impact on protein function.
openCRAVAT entries into a vcf file:
##INFO=<ID=OC_vest__transcript,Number=A,Type=String,Description="">
##INFO=<ID=OC_vest__score,Number=A,Type=Float,Description="">
##INFO=<ID=OC_vest__pval,Number=A,Type=Float,Description="">
##INFO=<ID=OC_vest__all,Number=A,Type=String,Description="">
ID=OC_vest__transcript: the transcript in which the variant is located
ID=OC_vest__score: ranges from 0 to 1, where 1 indicates a confident prediction of a functional mutation. 
ID=OC_vest__pval: 

4. SpliceAI

openCRAVAT entries into a vcf file:
##INFO=<ID=OC_spliceai__ds_ag,Number=A,Type=Float,Description="Probability of the variant being splice-altering. Cutoffs for binary prediction are, 0.2 (high recall), 0.5 (recommended), and 0.8 (high precision)">
##INFO=<ID=OC_spliceai__ds_al,Number=A,Type=Float,Description="Probability of the variant being splice-altering. Cutoffs for binary prediction are, 0.2 (high recall), 0.5 (recommended), and 0.8 (high precision)">
##INFO=<ID=OC_spliceai__ds_dg,Number=A,Type=Float,Description="Probability of the variant being splice-altering. Cutoffs for binary prediction are, 0.2 (high recall), 0.5 (recommended), and 0.8 (high precision)">
##INFO=<ID=OC_spliceai__ds_dl,Number=A,Type=Float,Description="Probability of the variant being splice-altering. Cutoffs for binary prediction are, 0.2 (high recall), 0.5 (recommended), and 0.8 (high precision)">
##INFO=<ID=OC_spliceai__dp_ag,Number=A,Type=Integer,Description="Location where splicing changes relative to the variant position (positive values are downstream of the variant, negative values are upstream).">
##INFO=<ID=OC_spliceai__dp_al,Number=A,Type=Integer,Description="Location where splicing changes relative to the variant position (positive values are downstream of the variant, negative values are upstream).">
##INFO=<ID=OC_spliceai__dp_dg,Number=A,Type=Integer,Description="Location where splicing changes relative to the variant position (positive values are downstream of the variant, negative values are upstream).">
##INFO=<ID=OC_spliceai__dp_dl,Number=A,Type=Integer,Description="Location where splicing changes relative to the variant position (positive values are downstream of the variant, negative values are upstream).">
Example, Taken from openCRAVAT
Acceptor Gain Score 	Acceptor Loss Score 	Donor Gain Score 	Donor Loss Score 	Acceptor Gain Position 	Acceptor Loss Position 	Donor Gain Position 	Donor Loss Position
0.00 			0.00 			0.91 			0.08 			-28 			-46 			-2 			-31

    The probability that the position 19:38958360 (=38958362-2) is used as a splice donor increases by 0.91.
    The probability that the position 19:38958331 (=38958362-31) is used as a splice donor decreases by 0.08.

5. REVEL - Rare Exome Variant Ensemble Learner
This is an ensemble method for predicting the pathogenicity of missense variants based on a combination of scores from 13 individual tools.
openCRAVAT entries into a vcf file:
##INFO=<ID=OC_revel__transcript,Number=A,Type=String,Description="Ensembl transcript ids">
##INFO=<ID=OC_revel__score,Number=A,Type=Float,Description="Max pathogenicity score across all transcripts.">
##INFO=<ID=OC_revel__rankscore,Number=A,Type=Float,Description="Max rank score across all transcripts. The ratio of the rank of the score over the total number of REVEL scores.">
##INFO=<ID=OC_revel__all,Number=A,Type=String,Description="">
OC_revel__score: the higher the score the more likely that the variant is pathogenic.
******

6. PROVEAN - Protein Variant Effect Analyzer
Predicts the functional impact of amino acid substitutions in proteins.
openCRAVAT entries into a vcf file:
##INFO=<ID=OC_provean__transcript,Number=A,Type=String,Description="Ensembl transcript ids">
##INFO=<ID=OC_provean__uniprot,Number=A,Type=String,Description="">
##INFO=<ID=OC_provean__score,Number=A,Type=Float,Description="Scores range from -14 to 14. The smaller the score the more likely the SNP has damaging effect.">
##INFO=<ID=OC_provean__rankscore,Number=A,Type=Float,Description="The ratio of the rank the PROVEAN score over the total number of PROVEAN scores.">
##INFO=<ID=OC_provean__prediction,Number=A,Type=String,Description="If score <= -2.5 (rankscore>=0.54382) the corresponding nsSNV is predicted as "D(amaging)"; otherwise it is predicted as "N(eutral)".">
##INFO=<ID=OC_provean__all,Number=A,Type=String,Description="">
****

7. MutPred - 
classifies amino acid substitution (AAS) as disease-associated or neutral in human.
openCRAVAT entries into a vcf file:
##INFO=<ID=OC_mutpred1__transcript,Number=A,Type=String,Description="Ensembl Transcript ID.">
##INFO=<ID=OC_mutpred1__external_protein_id,Number=A,Type=String,Description="Uniprot ID">
##INFO=<ID=OC_mutpred1__amino_acid_substitution,Number=A,Type=String,Description="">
##INFO=<ID=OC_mutpred1__mutpred_general_score,Number=A,Type=Float,Description="Max Pathogenicity score across all transcripts.">
##INFO=<ID=OC_mutpred1__mutpred_rankscore,Number=A,Type=Float,Description="The max rank score across all transcripts. The ratio of the rank of the score over the total number of MutPred scores.">
##INFO=<ID=OC_mutpred1__mutpred_top5_mechanisms,Number=A,Type=String,Description="">

general score:the probability that the amino acid substitution is deleterious/disease-associated


8. MutationTaster - 
openCRAVAT entries into a vcf file:
Evaluates the pathogenic potential of sequence alteration. It is designed to predict the functional consequences of AA substitution, intronic and synonymous alterations, short insertion and/ or deletion (indel) mutations and variants spanning intron-exon borders
##INFO=<ID=OC_mutationtaster__transcript,Number=A,Type=String,Description="Ensembl transcript ids">
##INFO=<ID=OC_mutationtaster__score,Number=A,Type=Float,Description="p-value, ranges from 0 to 1.">
##INFO=<ID=OC_mutationtaster__rankscore,Number=A,Type=Float,Description="The scores were first converted. If there are multiple scores of a SNV, only the largest converted score was used in ranking. The rankscore is the ratio of the rank of the converted score over the total number of converted scores. The scores range from 0.08979 to 0.81001.">
##INFO=<ID=OC_mutationtaster__prediction,Number=A,Type=String,Description="The score cutoff between "Disease Causing" and "Polymorphism" is 0.5 for converted score and 0.31733 for the rankscore.">
##INFO=<ID=OC_mutationtaster__model,Number=A,Type=String,Description="Predicted models.">
##INFO=<ID=OC_mutationtaster__all,Number=A,Type=String,Description="">

9. dbscSNV - 
openCRAVAT entries into a vcf file:
##INFO=<ID=OC_dbscsnv__ada_score,Number=A,Type=Float,Description="If the score >0.6, it predicts that the splicing will be changed, otherwise it predicts the splicing will not be changed.">
##INFO=<ID=OC_dbscsnv__rf_score,Number=A,Type=Float,Description="If the score >0.6, it predicts that the splicing will be changed, otherwise it predicts the splicing will not be changed">
