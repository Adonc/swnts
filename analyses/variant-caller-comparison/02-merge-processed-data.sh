
dir="../../scratch"

### Create file describing log files from all samples
#Parse log files to create a single file showing the overlap between the 2 callers

#Create header
echo -e "PatientNo\tDataSource\tCommonCalls\tUnique.DV\tUnique.HC" > ../../scratch/blood.toolComparisonSummary.tsv
while read -r sample; do
	#Extract patient number and source of data
	sample_id=$(echo ${sample} | grep -Po 'patient_\K[0-9]*')
	sample_source=$(echo ${sample} | sed 's/swn_patient_[0-9]*_//g' | sed 's/_.*//g')
	#Extract info from vcftools log file
	common=$(grep common ${dir}/${sample}.DV1_v_HC2.out.log | awk 'BEGIN{FS=OFS=" "}{print $2}')
	DV=$(grep "only in main" ${dir}/${sample}.DV1_v_HC2.out.log | awk 'BEGIN{FS=OFS=" "}{print $2}')
	HC=$(grep "only in second" ${dir}/${sample}.DV1_v_HC2.out.log | awk 'BEGIN{FS=OFS=" "}{print $2}')
	#Output info
	echo -e "${sample_id}\t${sample_source}\t${common}\t${DV}\t${HC}" >> ../../scratch/blood.toolComparisonSummary.tsv
done < ../../scratch/samples.txt


### Collect Transition vs Transition count information
#Create header
echo -e "PatientNo\tSource\tTransitions.DV\tTransitions.HC\tTransitions.Common\tTransversions.DV\tTransversions.HC\tTransversions.Common" > ../../scratch/blood.toolTiTvRatio.tsv

#The following parses a vcftools output file with the format: 
#CHROM	POS1	POS2	IN_FILE	REF1	REF2	ALT1	ALT2
#chr1	13273	.	1	G	.	C	.
#chr1	.	14677	2	.	G	.	A
#chr1	63074	63074	B	A	A	C	C
#columns pos1,ref1 and alt1 describe info from vcf 1 (DeepVariant)
#columns pos2,ref2 and alt2 describe info from vcf 2 (HaplotypeCaller)
#in_file has values [1,2,B] refering to files 1,2 or both

while read -r sample; do
	#Extract patient number and source of data, as done above
	sample_id=$(echo ${sample} | grep -Po 'patient_\K[0-9]*')
	sample_source=$(echo ${sample} | sed 's/.*patient_[0-9]*_//g' | sed 's/_.*//g')
	# Count the number of transitions and transversions,
	# and whether they are unique/shared between tools
	# This works by checking columns 5 vs 7 (DeepVariant ref vs alt) 
	# and columns 6 vs 8 (HaplotypeCaller ref vs alt).
	# Counts transitions and transversions, and if they match on that row,
	# increase the count for the common calls as well.
	# Transition: A<>G,C<>T
	# Transversion: C<>G,C<>G,A<>T,T<>G
	cat ${dir}/${sample}.DV1_v_HC2.out.diff.sites_in_files | 
 	awk -v s=${sample_id} -v so=${sample_source}\
        	'BEGIN{FS=OFS="\t"}{\
                if(($5=="A" && $7=="G") ||\
                ($5=="G" && $7=="A") ||\
                ($5=="C" && $7=="T") ||\
                ($5=="T" && $7=="C"))\
                {{ti1+=1}{ti1bool=1}}\
                if(($6=="A" && $8=="G") ||\
                ($6=="G" && $8=="A") ||\
                ($6=="C" && $8=="T") ||\
                ($6=="T" && $8=="C"))\
                {{ti2+=1}{ti2bool=1}}\
                if(ti1bool==1 && ti2bool==1)\
                {tib+=1}\
                if(($5=="A" && $7=="C") ||\
                ($5=="C" && $7=="A") ||\
                ($5=="C" && $7=="G") ||\
                ($5=="G" && $7=="C") ||\
                ($5=="A" && $7=="T") ||\
                ($5=="T" && $7=="A") ||\
                ($5=="T" && $7=="G") ||\
                ($5=="G" && $7=="T"))\
                {{tv1+=1}{tv1bool=1}}\
                if(($6=="A" && $8=="C") ||\
                ($6=="C" && $8=="A") ||\
                ($6=="C" && $8=="G") ||\
                ($6=="G" && $8=="C") ||\
                ($6=="A" && $8=="T") ||\
                ($6=="T" && $8=="A") ||\
                ($6=="T" && $8=="G") ||\
                ($6=="G" && $8=="T"))\
                {{tv2+=1}{tv2bool=1}}\
                if(tv1bool==1 && tv2bool==1)\
                {tvb+=1}\
                {ti1bool=0}\
                {ti2bool=0}\
                {tv1bool=0}\
                {tv2bool=0}}\
                END\
                {print s,so,ti1,ti2,tib,tv1,tv2,tvb}' \
	>> ../../scratch/blood.toolTiTvRatio.tsv
done < ../../scratch/samples.txt

