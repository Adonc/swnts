#! /bin/bash
#for this to work, ensure that the yml file is pointed to the correct ensembl build
#ie -bioconda::ensembl-vep=107 to bioconda::ensembl-vep=109
#if the libcrypt.so.1 error is thrown by perl, replace amazonlinux:latest with amazonlinux:2 in the dockerfile

while read line
do
  echo $line
  perl vcf2maf.pl --input-vcf /workdir/vcfs/$line --output-maf /workdir/test.vep.maf --ref-fasta /workdir/fasta/Homo_sapiens_assembly38.fasta --vep-path /root/miniconda3/envs/vcf2maf/bin --vep-data /workdir/vep --ncbi-build GRCh38
  file_name=${line%.vcf}
  gzip /workdir/vcfs/${file_name}.vep.vcf
  rm /workdir/vcfs/$line
done< vcf_files.txt 
